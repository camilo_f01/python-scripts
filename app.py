# This program is called "Random input >> output generation".
# it does random number generation between two integers that the user inputs.
# This program takes two arguments the first one should be less or equel than the second one both should be integers.
# Created by Camilo Ferrer aka TeKJoke.

import random

class my_class:
    def __init__(self, range1, range2):
        self.random_num = random.randint(range1, range2)

    def print_num(self):
        print(self.random_num)

def ask_num(order):
    # variable to use as a do while loop
    check_int = 0
    while check_int != 1:
        try:
            range_num = int(input(f"Give me the {order} number: "))
            check_int += 1
        except ValueError:
            print("Error please input a number!")

    return range_num

def compare_nums(range1, range2):
    if range1 <= range2:
        return True
    else:
        return False

def main():
    # variable to use for the order of ask_num function
    order = ["1st", "2nd"]

    print("Please input two numbers the first one must be less than or equel to the first one.")

    # variable to use as a do while loop
    check_num = 0
    while check_num != 1:
        index = 0
        range1 = ask_num(order[index])
        index += 1
        range2 = ask_num(order[index])

        if compare_nums(range1, range2) == True:
            class1 = my_class(range1, range2)
            class1.print_num()
            check_num += 1
        else:
            print("Error! Please input the numbers again.")

if __name__ == "__main__":
    main()
